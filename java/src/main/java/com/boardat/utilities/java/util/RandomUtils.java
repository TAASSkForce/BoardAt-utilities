package com.boardat.utilities.java.util;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;

/**
 * Contains utilities for the random extraction of elements.
 *
 */
public class RandomUtils {

	/**
	 * Returns a random element from the given {@link Collection}, an empty
	 * {@link Optional} if is empty.
	 * 
	 * @param ts
	 *            a Collection
	 * @return a {@link Optional} of a random element
	 * @throws NullPointerException
	 *             if the Collection is null
	 */
	public static <T> Optional<T> getRandomElement(Collection<? extends T> ts) {
		return ts.stream().skip(ThreadLocalRandom.current().nextInt(ts.size())).findFirst().map(Function.identity());
	}

	/**
	 * Returns a random value of the given enumeration.
	 * 
	 * @param clazz
	 *            the class of an Enum
	 * @return a random value
	 * @throws NullPointerException
	 *             if the class is null
	 */
	public static <E extends Enum<E>> E getRandomElement(Class<E> clazz) {
		return clazz.getEnumConstants()[ThreadLocalRandom.current().nextInt(clazz.getEnumConstants().length)];
	}

}
