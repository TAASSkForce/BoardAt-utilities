package com.boardat.utilities.java.util;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Contains utilities for functional programming.
 */
public class FunctionalUtils {

	/**
	 * Returns a {@link Predicate} equivalent to the lazy logical or of all the
	 * predicates contained in the given Stream in an arbitrary order.
	 * 
	 * @param predicates
	 *            a {@link Stream} of {@link Predicate}s
	 * @return the lazy or of the Predicates
	 * @throws NullPointerException
	 *             if the stream is null
	 */
	public static <T> Predicate<T> makeOr(Stream<? extends Predicate<? super T>> predicates) {
		return combinePredicates(predicates, (a, b) -> () -> a.get() || b.get(), Boolean.TRUE);
	}

	/**
	 * Returns a {@link Predicate} equivalent to the lazy logical and of all the
	 * predicates contained in the given Stream in an arbitrary order.
	 * 
	 * @param predicates
	 *            a {@link Stream} of {@link Predicate}s
	 * @return the lazy and of the Predicates
	 * @throws NullPointerException
	 *             if the stream is null
	 */
	public static <T> Predicate<T> makeAnd(Stream<? extends Predicate<? super T>> predicates) {
		return combinePredicates(predicates, (a, b) -> () -> a.get() && b.get(), Boolean.FALSE);
	}

	/**
	 * Performs an eager foldr on the given {@link Stream}.
	 * 
	 * @param xs
	 *            a finite {@link Stream}
	 * @param acc
	 *            the starting accumulator value
	 * @param op
	 *            the combine operation
	 * @return the result of the foldr operation
	 * @throws NullPointerException
	 *             if any parameter is null
	 */
	public static <T, U> U foldr(Stream<? extends T> xs, U acc, BiFunction<? super T, ? super U, ? extends U> op) {
		Objects.requireNonNull(xs);
		Objects.requireNonNull(acc);
		Objects.requireNonNull(op);
		return xs.<Function<U, U>>map((a) -> (b) -> op.apply(a, b)).reduce(Function.identity(), Function::compose)
				.apply(acc);
	}

	/**
	 * Performs an eager foldl on the given {@link Stream}.
	 * 
	 * @param xs
	 *            a finite {@link Stream}
	 * @param acc
	 *            the starting accumulator value
	 * @param op
	 *            the combine operation
	 * @return the result of the foldl operation
	 * @throws NullPointerException
	 *             if any parameter is null
	 */
	public static <T, U> U foldl(Stream<? extends T> xs, U acc, BiFunction<? super U, ? super T, ? extends U> op) {
		Objects.requireNonNull(xs);
		Objects.requireNonNull(acc);
		Objects.requireNonNull(op);
		return xs.<Function<U, U>>map((a) -> (b) -> op.apply(b, a)).reduce(Function.identity(), Function::andThen)
				.apply(acc);
	}

	private static <T> Predicate<T> combinePredicates(Stream<? extends Predicate<? super T>> predicates,
			BinaryOperator<Supplier<Boolean>> with, Boolean identity) {
		Objects.requireNonNull(predicates);
		Objects.requireNonNull(with);
		Objects.requireNonNull(identity);
		return predicates.reduce(a -> true, (a, b) -> (c -> with.apply(() -> a.test(c), () -> b.test(c)).get()),
				(a, b) -> (c -> with.apply(() -> a.test(c), () -> b.test(c)).get()));
	}
}
