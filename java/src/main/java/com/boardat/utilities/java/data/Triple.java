package com.boardat.utilities.java.data;

import java.util.Objects;

/**
 * Represents an immutable triple of not null data
 *
 * @param <T1>
 *            the type of the first element
 * @param <T2>
 *            the type of the second element
 * @param <T3>
 *            the type of the third element
 */
public class Triple<T1, T2, T3> {

	/**
	 * The first element of the triple
	 */
	public final T1 _1;
	/**
	 * The second element of the triple
	 */
	public final T2 _2;
	/**
	 * The third element of the triple
	 */
	public final T3 _3;

	/**
	 * Constructs a new Triple
	 * 
	 * @param _1
	 *            the first element
	 * @param _2
	 *            the second element
	 * @param _3
	 *            the third element
	 * @throws NullPointerException
	 *             if any parameter is null
	 */
	public Triple(T1 _1, T2 _2, T3 _3) {
		super();
		this._1 = Objects.requireNonNull(_1);
		this._2 = Objects.requireNonNull(_2);
		this._3 = Objects.requireNonNull(_3);
	}

	/**
	 * Returns a new Triple with the same elements but shifted to the right by one
	 * position.
	 * 
	 * @return a new Triple with the same elements but shifted to the right by one
	 *         position
	 */
	public Triple<T3, T1, T2> shiftr() {
		return new Triple<>(this._3, this._1, this._2);
	}

	/**
	 * Returns a new Triple with the same elements but shifted to the left by one
	 * position.
	 * 
	 * @return a new Triple with the same elements but shifted to the left by one
	 *         position
	 */
	public Triple<T2, T3, T1> shiftl() {
		return new Triple<>(this._2, this._3, this._1);
	}

	/**
	 * Returns a {@link Pair} with the left part of the Triple.
	 * 
	 * @return a Pair with the left part of the Triple
	 */
	public Pair<T1, T2> left() {
		return new Pair<>(this._1, this._2);
	}

	/**
	 * Returns a {@link Pair} with the elements at the sides of the Triple
	 * 
	 * @return a Pair with the elements at the sides of the Triple
	 */
	public Pair<T1, T3> sides() {
		return new Pair<>(this._1, this._3);
	}

	/**
	 * Returns a {@link Pair} with the right part of the Triple.
	 * 
	 * @return a Pair with the right part of the Triple
	 */
	public Pair<T2, T3> right() {
		return new Pair<>(this._2, this._3);
	}

	@Override
	public String toString() {
		return "<" + this._1.toString() + ", " + this._2.toString() + ", " + this._3.toString() + ">";
	}

	@Override
	public boolean equals(Object obj) {
		boolean output = this == obj;
		if (!output && obj != null && obj.getClass() == this.getClass()) {
			Triple<?, ?, ?> cobj = (Triple<?, ?, ?>) obj;
			output = this._1.equals(cobj._1) && this._2.equals(cobj._2) && this._3.equals(cobj._3);
		}
		return output;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this._1, this._2, this._3);
	}

}
