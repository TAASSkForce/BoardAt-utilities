package com.boardat.utilities.java.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;

/**
 * Contains utilities for {@link Exception}s
 *
 */
public class ExceptionUtils {

	/**
	 * Returns an {@link Optional} containing the result of {@link Callable#call()},
	 * or an empty Optional if the supplier throws an {@link Exception}
	 * 
	 * @param supplier
	 *            the {@link Callable} supplying the result
	 * @return an {@link Optional} of the result of the Callable
	 * @throws NullPointerException
	 *             if the Callable is null
	 */
	public static <T> Optional<T> ignoreException(Callable<T> supplier) {
		Optional<T> output;
		try {
			output = Optional.of(supplier.call());
		} catch (Exception e) {
			output = Optional.empty();
		}
		return output;
	}

	/**
	 * Returns the stack trace of the given {@link Throwable} as a String.
	 * 
	 * @param t
	 *            a {@link Throwable}
	 * @return the stack trace as a String
	 * @throws NullPointerException
	 *             if the Throwable is null
	 */
	public static String getStackTrace(Throwable t) {
		StringWriter sw = new StringWriter();
		Objects.requireNonNull(t).printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

	/**
	 * Returns an Optional containing the given object casted to the given class if
	 * possible, an empty Optional otherwise.
	 * 
	 * @param obj
	 *            an object
	 * @param toClass
	 *            a class
	 * @return optionally the casted object
	 */
	public static <T> Optional<T> safeCast(Object obj, Class<? extends T> toClass) {
		return Optional.of(obj).filter(toClass::isInstance).map(toClass::cast);
	}

}
