package com.boardat.utilities.java.util;

import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

/**
 * Contains utilities for Strings
 */
public class StringUtils {

	private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	/**
	 * Cut the given string at a maximum number of chars
	 * 
	 * @param s
	 *            the string
	 * @param chars
	 *            the max number of chars
	 * @return the cut String
	 * @throws NullPointerException
	 *             if the String is null
	 */
	public static String abbreviate(String s, int chars) {
		return abbreviate(s, chars, false);
	}

	/**
	 * Cut the given string at a maximum number of chars. If withDots is true the
	 * cut string will end with three dots if the string was cut (the three dot are
	 * included in the maximum length)
	 * 
	 * @param s
	 *            the string
	 * @param chars
	 *            the max number of chars
	 * @param withDots
	 *            true if three ending dots must be add in case of cuts
	 * @return the cut String
	 * @throws NullPointerException
	 *             if the String is null
	 */
	public static String abbreviate(String s, int chars, boolean withDots) {
		String output = Objects.requireNonNull(s);
		if (s.length() > chars) {
			output = s.substring(0, chars);
			if (withDots && !output.endsWith("...")) {
				output = output.substring(0, Math.max(chars - 3, 0)) + "...";
			}
		}
		return output;
	}

	/**
	 * Generate a random alphanumeric string with the given length
	 * 
	 * @param length
	 *            the required length
	 * @return a random string of the requested length
	 */
	public static String generateRandomString(int length) {
		return IntStream.range(0, length).map(i -> ThreadLocalRandom.current().nextInt(ALPHABET.length()))
				.mapToObj(ALPHABET::charAt).collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
				.toString();
	}

	/**
	 * Return the extension of the file with the dot
	 * 
	 * @param fileName
	 *            a String containing the full name of the file
	 * @return the file extension with the dot
	 */

	public static String getFileExtension(String fileName) {
		return fileName.substring(fileName.lastIndexOf("."));
	}
}
