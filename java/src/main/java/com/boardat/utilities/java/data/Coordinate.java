package com.boardat.utilities.java.data;

import java.util.Objects;

/**
 * Represents a geographical coordinate.
 *
 */
public class Coordinate {

	private final double latitude, longitude;

	/**
	 * Constructs a new Coordinates representing the given latitude and longitude
	 * 
	 * @param latitude
	 *            the latitude of the coordinate
	 * @param longitude
	 *            the longitude of the coordinate
	 */
	public Coordinate(double latitude, double longitude) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
	}

	/**
	 * Returns the latitude of the coordinate.
	 * 
	 * @return the latitude of the coordinate
	 */
	public double getLatitude() {
		return this.latitude;
	}

	/**
	 * Returns the longitude of the coordinate.
	 * 
	 * @return the longitude of the coordinate
	 */
	public double getLongitude() {
		return this.longitude;
	}

	@Override
	public final boolean equals(Object obj) {
		boolean output = this == obj;
		if (obj != null && obj.getClass() == this.getClass()) {
			Coordinate cobj = (Coordinate) obj;
			output = this.latitude == cobj.latitude && this.longitude == cobj.longitude;
		}
		return output;
	}

	@Override
	public final int hashCode() {
		return Objects.hash(Double.hashCode(this.latitude), Double.hashCode(this.longitude));
	}

	@Override
	public String toString() {
		return "Coordinate(latitude=" + this.latitude + ", longitude=" + this.longitude + ")";
	}

}
