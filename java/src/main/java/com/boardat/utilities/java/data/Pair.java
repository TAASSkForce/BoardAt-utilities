package com.boardat.utilities.java.data;

import java.util.Objects;

/**
 * Represents an immutable pair of not null data
 *
 * @param <T1>
 *            the type of the first element
 * @param <T2>
 *            the type of the second element
 */
public final class Pair<T1, T2> {

	/**
	 * The first element of the pair
	 */
	public final T1 _1;
	/**
	 * the second element of the pair
	 */
	public final T2 _2;

	/**
	 * Constructs a new Pair
	 * 
	 * @param _1
	 *            the first element
	 * @param _2
	 *            the second element
	 * @throws NullPointerException
	 *             if any parameter is null
	 */
	public Pair(T1 _1, T2 _2) {
		super();
		this._1 = Objects.requireNonNull(_1);
		this._2 = Objects.requireNonNull(_2);
	}

	/**
	 * Returns a new Pair with the elements swapped.
	 * 
	 * @return a new Pair with the elements swapped
	 */
	public Pair<T2, T1> swap() {
		return new Pair<>(this._2, this._1);
	}

	/**
	 * Returns a new {@link Triple} adding an element at the left of this Pair
	 * 
	 * @param left
	 *            the new element
	 * @return a new Triple adding an element at the left of this pair
	 * @throws NullPointerException
	 *             if the new element is null
	 */
	public <T> Triple<T, T1, T2> left(T left) {
		return new Triple<>(left, this._1, this._2);
	}

	/**
	 * Returns a new {@link Triple} adding an element at the right of this Pair
	 * 
	 * @param right
	 *            the new element
	 * @return a new Triple adding an element at the right of this pair
	 * @throws NullPointerException
	 *             if the new element is null
	 */
	public <T> Triple<T1, T2, T> right(T right) {
		return new Triple<>(this._1, this._2, right);
	}

	/**
	 * Returns a new {@link Triple} adding an element at the middle of this Pair
	 * 
	 * @param middle
	 *            the new element
	 * @return a new Triple adding an element at the middle of this pair
	 * @throws NullPointerException
	 *             if the new element is null
	 */
	public <T> Triple<T1, T, T2> middle(T middle) {
		return new Triple<>(this._1, middle, this._2);
	}

	@Override
	public String toString() {
		return "<" + this._1.toString() + ", " + this._2.toString() + ">";
	}

	@Override
	public boolean equals(Object obj) {
		boolean output = this == obj;
		if (!output && obj != null && obj.getClass() == this.getClass()) {
			Pair<?, ?> cobj = (Pair<?, ?>) obj;
			output = this._1.equals(cobj._1) && this._2.equals(cobj._2);
		}
		return output;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this._1, this._2);
	}

}
