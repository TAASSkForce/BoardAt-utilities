package com.boardat.utilities.spring.security.config;

import java.util.Map;
import java.util.Optional;

import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import com.boardat.utilities.java.util.ExceptionUtils;
import com.boardat.utilities.spring.security.data.BoardAtUserDetails;

public class DetailsExtractorJwtAccessTokenConverter extends JwtAccessTokenConverter {

	private static final String id = "id", username = "user_name", fullname = "fullname";

	@Override
	public OAuth2Authentication extractAuthentication(Map<String, ?> map) {
		OAuth2Authentication output = super.extractAuthentication(map);
		getDetails(map).ifPresent(output::setDetails);
		return output;
	}

	private static Optional<BoardAtUserDetails> getDetails(Map<String, ?> map) {
		Optional<BoardAtUserDetails> output = Optional.empty();
		if (map.get(id) != null && map.get(username) != null && map.get(fullname) != null) {
			output = ExceptionUtils.ignoreException(() -> Long.parseLong(map.get(id).toString()))
					.map(id -> new BoardAtUserDetails(id, map.get(username).toString(), map.get(fullname).toString()));
		}
		return output;
	}

}