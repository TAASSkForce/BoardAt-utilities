package com.boardat.utilities.spring.security.config;

import java.util.Objects;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.web.client.RestTemplate;

/**
 * Default configuration for a {@link OAuth2RestTemplate}
 */
public abstract class OAuth2RestTemplateConfiguration {

	private final String clientId;
	private final String clientSecret;
	private final String accessTokenUrl;

	/**
	 * Configures a {@link OAuth2RestTemplate} with the given configuration.
	 * 
	 * @param clientId
	 *            the id of the client
	 * @param clientSecret
	 *            the secret of the client
	 * @param accessTokenUrl
	 *            the token endpoint
	 */
	public OAuth2RestTemplateConfiguration(String clientId, String clientSecret, String accessTokenUrl) {
		super();
		this.clientId = Objects.requireNonNull(clientId);
		this.clientSecret = Objects.requireNonNull(clientSecret);
		this.accessTokenUrl = Objects.requireNonNull(accessTokenUrl);
	}

	/**
	 * Constructs a default {@link OAuth2RestTemplate} with the informations
	 * provided in the constructor.
	 * 
	 * @return a new {@link OAuth2RestTemplate}
	 */
	@BoardAtClient
	@Bean
	public OAuth2RestTemplate authRestTemplate() {
		ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
		resourceDetails.setId("1");
		resourceDetails.setClientId(this.clientId);
		resourceDetails.setClientSecret(this.clientSecret);
		resourceDetails.setAccessTokenUri(this.accessTokenUrl);
		OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails, new DefaultOAuth2ClientContext());
		return restTemplate;
	}

	/**
	 * Constructs a default {@link RestTemplate} without any authorization
	 * configuration
	 * 
	 * @return a default {@link RestTemplate}
	 */
	@Bean
	@Primary
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
