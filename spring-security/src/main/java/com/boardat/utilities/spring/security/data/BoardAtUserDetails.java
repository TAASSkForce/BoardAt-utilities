package com.boardat.utilities.spring.security.data;

import java.util.Objects;

/**
 * The account informations stored in the jwt token
 *
 */
public class BoardAtUserDetails {

	private final long id;
	private final String email;
	private final String fullname;

	/**
	 * Constructs a {@link BoardAtUserDetails} with the given id, email and fullname
	 * 
	 * @param id
	 *            the account id
	 * @param email
	 *            the account email
	 * @param fullname
	 *            the account fullname
	 */
	public BoardAtUserDetails(long id, String email, String fullname) {
		super();
		this.id = id;
		this.email = Objects.requireNonNull(email);
		this.fullname = Objects.requireNonNull(fullname);
	}

	/**
	 * Returns the id of the account.
	 * 
	 * @return the id of the account
	 */
	public long getId() {
		return this.id;
	}

	/**
	 * Returns the email of the account.
	 * 
	 * @return the email of the account
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Returns the full name of the account.
	 * 
	 * @return the full name of the account
	 */
	public String getFullName() {
		return this.fullname;
	}

	@Override
	public String toString() {
		return "BoardAtUserDetails(id=" + this.id + ",email=" + this.email + ", fullname=" + this.fullname + ")";
	}

}