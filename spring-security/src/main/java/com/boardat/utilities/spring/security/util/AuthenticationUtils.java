package com.boardat.utilities.spring.security.util;

import java.util.Optional;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

import com.boardat.utilities.java.util.ExceptionUtils;
import com.boardat.utilities.spring.security.data.BoardAtUserDetails;

/**
 * Contains utilities for extract authentication informations
 *
 */
public class AuthenticationUtils {

	/**
	 * Returns an {@link Optional} containing the {@link BoardAtUserDetails} stored
	 * in the {@link SecurityContextHolder} authentication if possible, an empty
	 * Optional otherwise.
	 * 
	 * @return optionally the BoardAtUserDetails
	 */
	public static Optional<BoardAtUserDetails> getUserDetails() {
		return AuthenticationUtils.getUserDetails(SecurityContextHolder.getContext().getAuthentication());
	}

	/**
	 * Returns an {@link Optional} containing the {@link BoardAtUserDetails} stored
	 * in the given {@link Authentication} if possible, an empty Optional otherwise.
	 * 
	 * @return optionally the BoardAtUserDetails
	 */
	public static Optional<BoardAtUserDetails> getUserDetails(Authentication auth) {
		return ExceptionUtils.safeCast(auth.getDetails(), OAuth2AuthenticationDetails.class)
				.map(OAuth2AuthenticationDetails::getDecodedDetails)
				.flatMap(obj -> ExceptionUtils.safeCast(obj, BoardAtUserDetails.class));
	}

	public boolean loggedAs(long id) {
		return AuthenticationUtils.getUserDetails().map(BoardAtUserDetails::getId)
				.filter(loggedId -> loggedId.equals(id)).isPresent();
	}

	public boolean logged() {
		return AuthenticationUtils.getUserDetails().isPresent();
	}

}
