package com.boardat.utilities.spring.security.util;

import org.springframework.security.access.prepost.PreAuthorize;

/**
 * Utilities for {@link PreAuthorize} annotations
 *
 */
public class PreAuthorizeConstants {

	/**
	 * Check is authenticated as an account
	 */
	public static final String IS_ACCOUNT = "hasRole('ROLE_ACCOUNT')";
	/**
	 * Check is authenticated as a user
	 */
	public static final String IS_USER = "hasRole('ROLE_USER')";
	/**
	 * Check is authenticated as a shop
	 */
	public static final String IS_SHOP = "hasRole('ROLE_SHOP')";
	/**
	 * Check is authenticated as an admin
	 */
	public static final String IS_ADMIN = "hasRole('ROLE_ADMIN')";
	/**
	 * Check is a BoardAt service
	 */
	public static final String IS_BOARDAT = "hasRole('ROLE_BOARDAT_CLIENT')";

}
