package com.boardat.utilities.spring.security.config;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import com.boardat.utilities.spring.security.util.AuthenticationUtils;

public class DefaultResourceServerConfiguration extends ResourceServerConfigurerAdapter {

	private final String signingKey;

	@Autowired
	public DefaultResourceServerConfiguration(String signingKey) {
		this.signingKey = Objects.requireNonNull(signingKey);
	}

	@Bean
	@Primary
	public DefaultTokenServices tokenServices() {
		DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
		defaultTokenServices.setTokenStore(tokenStore());
		return defaultTokenServices;
	}

	@Bean
	public TokenStore tokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}

	@Bean("auth")
	public AuthenticationUtils authUtil() {
		return new AuthenticationUtils();
	}

	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		DetailsExtractorJwtAccessTokenConverter converter = new DetailsExtractorJwtAccessTokenConverter();
		converter.setSigningKey(this.signingKey);
		return converter;
	}

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.tokenServices(tokenServices());
	}
}