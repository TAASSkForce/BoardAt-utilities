package com.boardat.utilities.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CoordinatesMixin {

	@JsonCreator
	public CoordinatesMixin(@JsonProperty("latitude") double latitude, @JsonProperty("longitude") double longitude) {
		// mixin
	}

}
