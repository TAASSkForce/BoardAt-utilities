package com.boardat.utilities.spring.data.jackson;

import java.util.Collections;

import org.springframework.data.domain.Sort;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeserializableSort extends Sort {

	private static final long serialVersionUID = 1L;

	// TODO create a true serialization and deserialization for sort

	@SuppressWarnings("deprecation")
	@JsonCreator
	public DeserializableSort() {
		super(Collections.emptyList());
	}

}
