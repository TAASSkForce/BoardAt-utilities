package com.boardat.utilities.spring.data.util;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.StreamSupport;

import org.springframework.data.repository.CrudRepository;

/**
 * Constains utilities for {@link CrudRepository}s
 *
 */
public class CrudRepositoryUtil {

	/**
	 * Returns a random entity from the given {@link CrudRepository}, an empty
	 * {@link Optional} if is empty.
	 * 
	 * @param repo
	 *            a {@link CrudRepository}
	 * @return a random entity
	 * @throws NullPointerException
	 *             if the repository is null
	 */
	public static <T> Optional<T> getRandomExistingEntity(CrudRepository<? extends T, ?> repo) {
		return StreamSupport.stream(repo.findAll().spliterator(), false)
				.skip(ThreadLocalRandom.current().nextLong(repo.count())).findFirst().map(Function.identity());
	}

	/**
	 * Returns a supplier of random entities from the given {@link CrudRepository}.
	 * The supplier will return an empty {@link Optional} if is called while the
	 * repository is empty.
	 * 
	 * @param repo
	 *            a CrudRepository
	 * @return a supplier of random entities
	 * @throws NullPointerException
	 *             if the repository is null
	 */
	public static <T> Supplier<Optional<T>> getRandomExistingEntitySupplier(CrudRepository<? extends T, ?> repo) {
		Objects.requireNonNull(repo);
		return () -> CrudRepositoryUtil.getRandomExistingEntity(repo);
	}

}
