package com.boardat.utilities.spring.data.config;

import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.config.PageableHandlerMethodArgumentResolverCustomizer;

public interface PagingDefaultConfiguration extends PageableHandlerMethodArgumentResolverCustomizer {

	@Override
	default void customize(PageableHandlerMethodArgumentResolver pageableResolver) {
		pageableResolver.setMaxPageSize(50);
	}

}
