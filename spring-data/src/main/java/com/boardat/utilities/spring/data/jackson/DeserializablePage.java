package com.boardat.utilities.spring.data.jackson;

import java.util.List;

import org.springframework.data.domain.PageImpl;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeserializablePage<T> extends PageImpl<T> {

	private static final long serialVersionUID = 1L;

	@JsonCreator
	public DeserializablePage(@JsonProperty("content") List<T> content,
			@JsonProperty("pageable") DesetializablePageable pageable, @JsonProperty("total") long total) {
		super(content, pageable, total);
	}

	public DeserializablePage(List<T> content) {
		super(content);
	}

}
