package com.boardat.utilities.spring.data.jackson;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DesetializablePageable implements Pageable {

	private final PageRequest page;

	@JsonCreator
	public DesetializablePageable(@JsonProperty("pageNumber") int pageNumber, @JsonProperty("pageSize") int pageSize,
			@JsonProperty("sort") DeserializableSort sort) {
		super();
		this.page = PageRequest.of(pageNumber, pageSize, sort);
	}

	@Override
	public int getPageNumber() {
		return this.page.getPageNumber();
	}

	@Override
	public int getPageSize() {
		return this.page.getPageSize();
	}

	@Override
	public long getOffset() {
		return this.page.getOffset();
	}

	@Override
	public Sort getSort() {
		return this.page.getSort();
	}

	@Override
	public Pageable next() {
		return this.page.next();
	}

	@Override
	public Pageable previousOrFirst() {
		return this.page.previousOrFirst();
	}

	@Override
	public Pageable first() {
		return this.page.first();
	}

	@Override
	public boolean hasPrevious() {
		return this.page.hasPrevious();
	}

}
