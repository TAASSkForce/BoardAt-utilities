package com.boardat.utilities.spring.core.converters;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * {@link JacksonMixinRegister} provide an autowired method for register mixins.
 */
public abstract class JacksonMixinRegister {

	private final Map<Class<?>, Class<?>> mixingRegistry;

	/**
	 * Constructs a {@link JacksonMixinRegister} that will register all the mixin
	 * contained in the given Map from the target class to the corresponding mixin.
	 * 
	 * @param mixingRegistry
	 *            a Map from a class to its mixin
	 * @throws NullPointerException
	 *             if the Map is null
	 */
	public JacksonMixinRegister(Map<Class<?>, Class<?>> mixingRegistry) {
		super();
		this.mixingRegistry = Collections.unmodifiableMap(new HashMap<>(mixingRegistry));
	}

	/**
	 * Registers all the mixin provided in the constructor.
	 * 
	 * @param mapper
	 *            an {@link ObjectMapper}
	 * @throws NullPointerException
	 *             if the ObjectMapper is null
	 */
	@Autowired
	public void registerMixins(ObjectMapper mapper) {
		Objects.requireNonNull(mapper);
		this.mixingRegistry.entrySet().forEach(entry -> {
			mapper.addMixIn(entry.getKey(), entry.getValue());
		});
	}

	/**
	 * Returns an immutable Map from the registered class to the corresponding
	 * registered mixin.
	 * 
	 * @return an immutable Map from a class to its mixin
	 */
	protected Map<Class<?>, Class<?>> getMixingRegistry() {
		return this.mixingRegistry;
	}

}
