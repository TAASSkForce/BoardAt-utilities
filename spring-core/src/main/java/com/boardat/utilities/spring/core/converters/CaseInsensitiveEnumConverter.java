package com.boardat.utilities.spring.core.converters;

import java.util.Arrays;
import java.util.Objects;

import org.springframework.core.convert.converter.Converter;

/**
 * Add supports to case insensitive enums parameters
 *
 * @param <T>
 *            the enum to be matched case insensitive
 */
public abstract class CaseInsensitiveEnumConverter<T extends Enum<T>> implements Converter<String, T> {

	private final Class<T> enumType;

	/**
	 * Constructs a CaseInsensitiveEnumConverter to support case insensitive
	 * parameters for the given enum class
	 * 
	 * @param enumType
	 *            the class of the enum
	 * @throws NullPointerException
	 *             if the class in null
	 */
	protected CaseInsensitiveEnumConverter(Class<T> enumType) {
		super();
		this.enumType = Objects.requireNonNull(enumType);
	}

	@Override
	public T convert(String source) {
		return Arrays.asList(this.enumType.getEnumConstants()).stream().filter(t -> t.name().equalsIgnoreCase(source))
				.findAny().orElseThrow(IllegalArgumentException::new);
	}

}
