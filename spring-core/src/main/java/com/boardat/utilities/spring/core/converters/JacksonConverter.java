package com.boardat.utilities.spring.core.converters;

import java.io.IOException;
import java.util.Objects;

import org.springframework.core.convert.converter.Converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

public abstract class JacksonConverter<T> implements Converter<String, T> {

	private final ObjectReader reader;

	protected JacksonConverter(ObjectMapper mapper, Class<T> clazz) {
		super();
		this.reader = Objects.requireNonNull(mapper).readerFor(clazz);
	}

	@Override
	public T convert(String source) {
		try {
			return this.reader.readValue(source);
		} catch (IOException e) {
			throw new IllegalArgumentException();
		}
	}

}
